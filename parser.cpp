#include "parser.h"
#include <QTextStream>
#include <QFile>
#include <QThread>
#include <QDebug>

Parser::Parser(const QString &filePath):
    _filePath(filePath)
{
}

void Parser::parse()
{
    QFile file("file.txt");
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
        {
            qDebug() << "нет файла";
            return;
        }


    QTextStream in(&file);
    int i = 0;
    while (!in.atEnd()) {
        QString word;
        in >> word;

        word.toLower();
        if (_dict.contains(word))
            ++_dict[word];
        else
            _dict.insert(word,1);
        ++i;
        if (i%5==0)
        {
            sort();
            emit takeChunk(_sortDict);
            //TODO: Сделать прослойку, в которую скидывать чанки с данными,
            //а отображение подбирало бы их от туда.
            QThread::msleep(1);
        }
    }

}

void Parser::sort()
{
    _sortDict.clear();
    for (auto it=_dict.constBegin(); it!=_dict.constEnd(); ++it)
    {
        _sortDict.append(QPair<int,QString>(it.value(),it.key()));
    }

    std::sort(_sortDict.begin(),_sortDict.end(),[](QPair<int,QString>& lhs,QPair<int,QString>& rhs){return lhs.first>rhs.first;});

    if (_sortDict.size()>15)
        _sortDict.resize(15);

}
