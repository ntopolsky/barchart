#include <QTextStream>
#include <QDebug>

#include "parser.h"
#include "mainwindow.h"
#include <holder.h>
#include "QThread"
#include "QObject"
#include <QtConcurrent>

void parsing(Parser* parser)
{
    parser->parse();
}

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    //TODO: button for choose file
    Parser parser("file.txt");
    MainWindow mainWindow;

    qRegisterMetaType<QVector<QPair<int,QString>>>();

    QObject::connect(&parser, &Parser::takeChunk, &mainWindow, &MainWindow::updateChart);

    QtConcurrent::run(parsing, &parser);

    return a.exec();
}
