#ifndef HOLDER_H
#define HOLDER_H

#include <QObject>

class Holder : public QObject
{
    Q_OBJECT
public:
    Holder();
    QVector<QPair<int, QString>> takePart();

public slots:
    void holdPart(QVector<QPair<int, QString>> stack);

signals:
    void stackIsNotEmpty();

private:
    QList<QVector<QPair<int, QString>>> _stack;
};

#endif // HOLDER_H
