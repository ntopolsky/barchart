#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QtWidgets/QApplication>
#include <QtWidgets/QMainWindow>
#include <QtCharts/QChartView>
#include <QtCharts/QBarSeries>
#include <QtCharts/QBarSet>
#include <QtCharts/QLegend>
#include <QtCharts/QBarCategoryAxis>
#include <QtCharts/QValueAxis>
#include <QObject>

QT_CHARTS_USE_NAMESPACE

class MainWindow : public QObject
{
    Q_OBJECT
public:
    MainWindow();

public slots:
    void updateChart(QVector<QPair<int, QString>> vec);

private:
    QBarSeries* _series;
    QBarCategoryAxis* _axisX;
    QValueAxis* _axisY;
    QChart* _chart;
    QChartView* _chartView;
    QMainWindow _window;
};

#endif // MAINWINDOW_H
