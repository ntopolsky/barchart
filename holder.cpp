#include "holder.h"

Holder::Holder()
{
}

QVector<QPair<int, QString> > Holder::takePart()
{
    return _stack.takeFirst();
}

void Holder::holdPart(QVector<QPair<int, QString> > stack)
{
    _stack.append(stack);
    emit stackIsNotEmpty();
}
