#include "mainwindow.h"
#include "QDebug"

MainWindow::MainWindow():
    _series(new QBarSeries()),
    _axisX(new QBarCategoryAxis()),
    _axisY(new QValueAxis()),
    _chart(new QChart()),
    _chartView(new QChartView())
{
    _chart->setTitle("Top 15 words");
    _chart->setAnimationOptions(QChart::NoAnimation);
    _chart->legend()->setVisible(true);
    _chart->legend()->setAlignment(Qt::AlignBottom);
    _chart->addSeries(_series);
    QStringList categories;
    categories << "Words";
    _axisX->setCategories(categories);
    _chart->addAxis(_axisX, Qt::AlignBottom);
    _series->attachAxis(_axisX);
    _chart->addAxis(_axisY, Qt::AlignLeft);
    _series->attachAxis(_axisY);
    _axisY->applyNiceNumbers();
    _chartView->setChart(_chart);
    _chartView->setRenderHint(QPainter::Antialiasing);

    _window.setCentralWidget(_chartView);

    _window.resize(750, 750);
    _window.show();
}

void MainWindow::updateChart(QVector<QPair<int, QString>> vec)
{
    QBarSeries* series = new QBarSeries();
    for (int i=0; i<vec.size();++i)
    {
        QBarSet *set = new QBarSet(vec[i].second);
        *set << vec[i].first;
        series->insert(i,set);
    }
    _chart->removeAllSeries();
    series->setBarWidth(1);
    _chart->addSeries(series);
    _axisY->setRange(0,vec[0].first);

}
