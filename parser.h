#ifndef PARSER_H
#define PARSER_H
#include <QString>
#include <QMap>
#include <QObject>

class Parser: public QObject
{
    Q_OBJECT
public:
    Parser(const QString& filePath);
    void parse();
    void sort();

signals:
    void takeChunk(QVector<QPair<int, QString>> values);

private:
    QString _filePath;
    QMap<QString,int> _dict;
    QVector<QPair<int, QString>> _sortDict;
};

#endif // PARSER_H
